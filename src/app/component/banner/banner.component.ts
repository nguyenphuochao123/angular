import { Component, OnInit } from '@angular/core';
import { Banner } from 'src/app/models/banner';
import { BannerService } from 'src/app/services/banner.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  banners:Array<Banner>=new Array<Banner>();

  constructor(private bn:BannerService){}
  ngOnInit(): void {
    this.bn.getBanners().subscribe(res=>{
      this.banners=res;
    })
  }

}
