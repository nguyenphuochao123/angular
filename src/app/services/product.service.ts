import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  //Danh sách
  //GET http://localhost:3000/product
  constructor(private http: HttpClient) { }
  getProduct(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/product');
  }
  // Thêm mới 
  //POST http://localhost:3000/product
  create(data: any): Observable<any> {
    return this.http.post<any>('http://localhost:3000/product', data);
  }
  //Xóa
  //DELETE http://localhost:3000/product/1
  delete(id: number): Observable<any> {
    return this.http.delete<any>('http://localhost:3000/product/' + id);
  }
  //cập nhật
  //PUT http://localhost:3000/product/1
  update(id:number,data: any): Observable<any> {
    return this.http.put<any>('http://localhost:3000/product/'+id, data);
  }
  //đọc 1 dòng
  //GET http://localhost:3000/product/1
  getone(id:number): Observable<any> {
    return this.http.get<any>('http://localhost:3000/product/'+id);
  }

}
