import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Banner } from '../models/banner';
import { Observable, throwError } from 'rxjs';
const _api='http://localhost:3000/';
@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private httpClient:HttpClient) { }

  getBanners(): Observable <Array<Banner>>{
    return this.httpClient.get<Array<Banner>>(_api+'banner');
  }
}
