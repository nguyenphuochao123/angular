import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
  id: number = 0;
  constructor(private proService: ProductService, private _route: ActivatedRoute) { }
  productFormUpdate: FormGroup = new FormGroup({
    name: new FormControl(),
    price: new FormControl(),
    image: new FormControl()
  });
  ngOnInit(): void {
    this.id = this._route.snapshot.params['id'];

    this.proService.getone(this.id).subscribe(data => {
      this.productFormUpdate = new FormGroup({
        name: new FormControl(data.name),
        price: new FormControl(data.price),
        image: new FormControl(data.image)
      });
    })

  }
  onUpdate(){
    this.proService.update(this.id,this.productFormUpdate.value).subscribe(data=>{
      console.log(data);
    });
  }

}
