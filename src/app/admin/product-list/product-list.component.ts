import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products:Array<any>=[];
  constructor(private proService:ProductService){}

  ngOnInit():void{
    this.proService.getProduct().subscribe(data=>{
      this.products=data;
    })
  }
  onDelete(id:number){
    if(confirm('Bạn chắc xóa chứ')){
    this.proService.delete(id).subscribe(data=>{
      this.proService.getProduct().subscribe(data=>{
        this.products=data;
      });
    });
  }
  }
}
